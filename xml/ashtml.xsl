<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    <xsl:template match="/">
        <html>
            <body>
                <p>
                    <xsl:for-each select="figures/view">
                        <img>
                            <xsl:attribute name="src">
                                <xsl:value-of select="."/>
                            </xsl:attribute> 
                            <xsl:attribute name="width">
                                500px
                            </xsl:attribute>
                        </img>
                    </xsl:for-each>
                </p>
            </body>
        </html>
    </xsl:template>   
</xsl:stylesheet>

