function saveWindows(fHandle,name,windows)
%      fHandle = gcf
% e.g. formatstr = 'meassurement_%03i.pdf'
%      typestr   = '-depsc'

%% TODO: add as second line <?xml-stylesheet type="text/xsl" href="ashtml.xsl"?>

figure(fHandle);
docNode = com.mathworks.xml.XMLUtils.createDocument('figures');
figures = docNode.getDocumentElement();

oldVisible = get(fHandle, 'Visible');
set(fHandle, 'Visible', 'off');
oldmode = xlim('mode');
oldview = xlim();

ext = 'svg';

h = waitbar(0,'saving...');
for i = 1:numel(windows)-1
    
    xlim([windows(i) windows(i+1)]);
    
    filename = sprintf([name,'_%03i.',ext],i);
    print(fHandle, filename, ['-d',ext]);
    waitbar(i/(numel(windows)-1),h)
    
    winElement = docNode.createElement('view');
    winElement.setAttribute('start',sprintf('%f',windows(i)  ));
    winElement.setAttribute('end',  sprintf('%f',windows(i+1)));
    winElement.appendChild(docNode.createTextNode(filename));
    figures.appendChild(winElement);
    
end
xmlwrite([name,'.xml'],docNode);
xslt([name,'.xml'],'ashtml.xsl',[name,'.html'])
close(h)


xlim(oldmode);
xlim(oldview);
set(fHandle, 'Visible', oldVisible);


end