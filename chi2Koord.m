function [ C, Ccrit, chi2, chi2crit ] = chi2Koord( dots2,AA, a1, a2 )
%[ C, Ccrit, chi2, chi2crit  ] = chi2Koord( dots2, AAintervals, edges )
%[ C, Ccrit, chi2, chi2crit  ] = chi2Koord( dots2, AAintervals, k,z )
%
% dots2     delta t values, dots(:,2)
% A         breath to breath intervals
%
% chi2 is dependent on number of bins and number of given beats
% C depends only on the number of bins
%
% plot when no return value

if nargin == 3 && numel(a1) > 1
    edges = a1;
else
    k = a1;
    z = a2;
    edges = linspace(0,z,k+1);
end


doPlot = nargout == 0;

N = numel(min(edges) <= dots2 & dots2 <= max(edges));
nA = numel(AA);
counts = histcounts(dots2,edges,'Normalization','count'); % values outside 
                                                % the interval are ignored
nC = numel(edges)-1;
E = N/nC; % expected/mean number of beats per bin


if E < 20
    fprintf('%i beats in plot\n',N)
    fprintf('%.1f avg beats per class\n',E)
    warning('there should be at least 20 points per class')
end

% Korrektur aufgrund der nicht konstanten Atemzugsdauer heartbeats
abortingA = histcounts(AA,edges,'Normalization','count');

% number of intervals ending in each class
nCorr = cumsum(abortingA) - 0.5*abortingA;
cCorr = counts * nA ./ (nA-nCorr);
Ecorr = sum(cCorr)/nC;

chi2 = sum((cCorr - Ecorr).^2) ./ Ecorr;
% formula (1) in the paper
% ciritical value: (chi2 must be bigger for coordination)
chi2crit = chi2inv(0.95,nC-1);

C = chi2 / Ecorr;
Ccrit = chi2crit / Ecorr;

if (doPlot)
    histogram(dots2,edges);
    hold on;
    stairs(edges,[cCorr cCorr(end)],':')
    plot([min(edges) max(edges)],E*[1 1],'-')
    plot([min(edges) max(edges)],Ecorr*[1 1],'-')
    hold off
end

end

