%% Koordination mit Fourier-Quantifikation

%% Verwendete Daten
% Die Daten wurden mit dem Modell 4 simmuliert und die Koordination soll
% nun mittels Fourierkoeffizienten analysiert werden.

load model4;
t_r = t_r(10:end);
[dots, Aind] = Koord(t_a,t_r);
plot(dots(:,1),dots(:,2),'.')
xlabel('Zeit t');
ylabel('Offset \Delta t')


%% Farbdarstellung des Koordigramms

fensterRadius=5; % Anzahl der Atemz�ge f�r das gleitenden Fensters, indem 
                 % die Vereilungenen bestimmt werden
bandbreite=0.1;  % Bandbreite des Kernsch�tzers zur Bestimmung der 
                 % Verteilungen (Zweifaches des Abtastschrittes der Atmung
yAxis = -8.0:0.05:8; % Einteilung der y-Achse
zVals =  4:0.05:8; % Variation der yAchsen-Grenze f�r die Korrektur


[verteilung,usedA] = colorcodedKoord(dots(:,2) ,Aind,...
                                   fensterRadius, yAxis, bandbreite);
h=imagesc(t_a(usedA), yAxis, verteilung);
colormap jet; caxis([0 1]); colorbar;
title('colored coordigram')
xlabel('Zeit t');
ylabel('Offset \Delta t')

%%
% zum Vergleich:

hold on
plot(dots(:,1),dots(:,2),'w.')
hold off

%% Fourier-Feld

fborder = [0.2 4];
[maxVal,maxF,fourier,f] = calcMaxFourier2(verteilung,yAxis,fborder);
%h=pcolor(fourier);

subplot(4,1,[1 2])
ff = min(fborder) <= f & f <= max(fborder);
h=pcolor(t_a, f(ff), fourier(ff,:));
set(h, 'EdgeColor', 'none'); ylim([0 max(fborder)]); 
xlabel('Zeit t')
ylabel('Frequenzband f')
colorbar;
set(gca,'Position',get(gca,'Position')); % Trick to align plots with colorbar

subplot(413)
plot(t_a,maxVal); ylabel('C(f_{max})')
subplot(414)
plot(t_a,maxF,':'); ylim(fborder);
xlabel('Zeit t')
ylabel('f_{max}')

%% mit Korrektur
fborder = [0.2 4];
[maxVal,maxF,fourier,f,usedZ,zFieldVal,zFieldF] = calcMaxFourier(verteilung,yAxis,fborder,zVals);
%h=pcolor(fourier);

subplot(4,1,[1 2])
ff = min(fborder) <= f & f <= max(fborder);
h=pcolor(t_a, f(ff), fourier(ff,:));
set(h, 'EdgeColor', 'none'); ylim([0 max(fborder)]); 
xlabel('Zeit t')
ylabel('Frequenzband f')
colorbar;
set(gca,'Position',get(gca,'Position')); % Trick to align with colorbar

subplot(413)
plot(t_a,maxVal); ylabel('C(f_{max})')
subplot(414)
plot(t_a,maxF,':'); ylim(fborder);
xlabel('Zeit t')
ylabel('f_{max}')

%%

clf
imagesc(1:numel(t_a),zVals,zFieldVal)
hold on
plot(usedZ)
axis xy
hold off

%%

clf
imagesc(1:numel(t_a),zVals,zFieldF)
hold on
plot(usedZ)
axis xy
hold off


%% Abh�ngigkeit vom Threshold
clf
threshold = 0.1;

is_coordinated = maxVal >= threshold;
fprintf('%.1f %% are above the threshold\n',100*sum(is_coordinated)/numel(is_coordinated))

h=imagesc(t_a, f(ff), fourier(ff,:));

hold on; % Add banner indicating coordinated beats
h=imagesc(t_a,[0 min(f(ff))], repmat(threshold*is_coordinated,2,1));
hold off;

ylim([0 max(fborder)]); 
xlabel('Zeit t')
ylabel('Frequenzband f')


%%
Cp = zeros(1,50);
n = 50;
th_val = linspace(0,max(maxVal),n);
for i = 1:n
    is_coordinated = maxVal >= th_val(i);
    Cp(i) = mean(is_coordinated); %sum(is_coordinated)/numel(is_coordinated);
end
plot(th_val,100*Cp,'b.:')
grid on
xlabel('Fourier coefficient threshold');
ylabel('percentage of coordination')

%%

clf;
subplot(211)
plot(dots( :,1), dots( :,2), 'k.' )
hold on
h = imagesc(t_a,[7 -7],repmat(maxVal,2,1));
set(h, 'AlphaData', 0.4);
uistack(h,'bottom');
xlim([min(t_r) max(t_r)]); colorbar;
colormap jet; %caxis([0 1]);
%load cMap; colormap(cMap); caxis([0 1]);

hold off 
set(gca,'Position',get(gca,'Position').*[1 1 0.9 1]); % Trick to align with colorbar
ylim([-1 1]*8)

subplot(212)
plot(t_a, maxVal);
set(gca,'Position',get(gca,'Position').*[1 1 0.9 1]); % Trick to align with colorbar
xlabel('Zeit t'); ylim([0 1]); grid on;
ylabel('max. coefficent C(f_{max})')


