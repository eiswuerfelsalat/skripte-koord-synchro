%% Shannon Entropie

%% Verwendete Daten
% Die Daten wurden mit dem Modell 4 simmuliert und die Koordination soll
% nun mit der Funktion ShannonEntropy berechnet und die Ergebnisse
% interpretiert werden.

load model4;
dots = Koord(t_a,t_r);
plot(dots(:,1),dots(:,2),'.')
xlabel('Zeit t');
ylabel('Offset \Delta t')

%% Auswertung mittels Shannon Entropie
% Es gibt eine Abhängigkeit von Binanzahl des Histograms, wie man im
% nachfolgenden Plot erkennt.

histogram(dots(:,2),40)
title('N = 40');
xlabel('\Delta t')
%%
histogram(dots(:,2),80)
title('N = 80')
xlabel('\Delta t')

%%
% Im Folgenden wird die daraus resultierende Abhängigkeit des
% Entropiewertes dargestellt. Der berechnete Wert wird zum einen mit einer
% Gleich- und der Normalverteilung verglichen. Die Gleichverteilung
% entspricht dem Entropiewert ohne Koordination.

n = 2:5:350;
r = [rand(size(dots(:,2))), randn(size(dots(:,2)))];
s = zeros(size(n));
sr = zeros(numel(n),2);
for i = 1:numel(n)
    s(i)   = ShannonEntropy(dots(:,2),n(i));
    sr(i,1)= ShannonEntropy(r(:,1),n(i));
    sr(i,2)= ShannonEntropy(r(:,2),n(i));
end

clf
plot(n,s,'x-',...
     n,sr(:,1),':',...
     n,sr(:,2),'.-')
xlabel('Binanzahl N')
ylabel('Shannon-Entropie S(N)')
 
legend({'daten','gleichverteilt','normal'},...
       'Location','se')

%%   
% In der Darstellung sind 3 Bereiche zu erkennen. Bei einer kleinen 
% Binanzahl werden die Häufungspunkte noch nicht aufgelöst und daher folgt
% der Entropiewert der Daten dem der Gleichverteilung. Mit zunehmender
% Anzahl werden diese aufgelöst. Die charakteristische Amplitude der 
% Häufungspunkte resultiert aus der zunehmenden Breite und damit abnehmenden Höhe
% der äußeren Maxima. Im
% letzen Bereich bleibt der Entropiewert konstant, weil die Binbreite die
% Samplinggenauigkeit unterschreitet.


%% Zeitabhängigkeit

winLen = 60; % sekunden
winStep = min(dots(:,1)):20:max(dots(:,1));
Se = zeros(size(winStep));

N = 60;
epsTh = 0.15;
for i = 1:numel(winStep)
    ii =  winStep(i)-winLen/2 <= dots(:,1) ...
          & dots(:,1) <= winStep(i)+winLen/2;
    Se(i) = ShannonEntropy(dots(ii,2),N);
end
SeNorm = Se / ShannonEntropy(r(:,1),N);

% data
plot(dots( :,1), dots( :,2), 'k.' )
hold on
    h = imagesc(winStep ,[-8 8],repmat(1-(SeNorm),2,1));
    set(h, 'AlphaData', 0.7); uistack(h,'bottom');
    colorbar; colormap jet; %caxis([0 1]*0.2);
    xlim([min(dots( :,1)) max(dots( :,1))]);

    % window
    plot(mean(winStep(2:3))+[0 winLen],[7 7],'r+-')
hold off 
ylim([-10 10])

xlabel('Zeit t')
title('Shannon-Entropie, color = 1-S/S(norm)')

