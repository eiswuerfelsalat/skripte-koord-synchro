function [ epsilons ] = calculate_epsilons( y, Aind, nb, mode )
%
% [ epsilons ] = calculate_epsilons( y, Aind )
%
%       y   :       timeoffset for every event
%                   e.g from dots = Koord(A,R) then use dots(:,2)
%       Aind:       index of corresponding trigger
%                   e.g use second output from Koord(A,R)
%
%   output
%       epsilons:   epsilon value for each heartbeat
%                   notice: the values for the last breath will be NaN
%
%   optional arguments
%
% [ epsilons ] = calculate_epsilons( y, Aind, N, mode )
%       N:          default N = 1
%                   parameter to specify how many next breaths should be
%                   condidered for distance calculation
%       
%       mode:       ["min", "max", "prod", "sum"] default: "min"
%                   when using N > 1 the returned epsilon is a lower (min)
%                   or a upper (max) limit.
%
%                   example:
%                   y = [1, 1.1, 1.2, 1.3]' and Aind = [1 2 3 3]'
%                   calculate_epsilons(y, Aind, 2, mode)
%                   the retured epsilon for y(1) is either 0.1 (min) or
%                   0.2 (max)

    assert(numel(y)==numel(Aind));
    epsilons  = NaN(size(y));
    n = numel(y);
    Aind = Aind(:); % column vector
    y = y(:); % column vector
    
    % consider nb next breath 
    if nargin < 3 
        nb = 1;
    else
        nb = 1:nb;
    end
    if nargin < 4 
        mode = 'max';
    end
    
    max_Aind = max(Aind);
    for yi = 1:n
        current_Aind = Aind(yi);
        if current_Aind == max_Aind
            % skip the last breath, but caution the y might not be sorted!
            continue;
        end
        
        % get indices of considered values
        for k = 1:numel(nb)
            ii = (Aind == current_Aind + nb(k));
            
            % compare and pick smallest for that breath
            e_candidate = min(abs(y(ii)-y(yi)));
            if numel(e_candidate) == 0
                continue
            end
            
            switch mode
                case 'min'
                    epsilons(yi) = min(epsilons(yi),e_candidate);
                case 'max'
                    epsilons(yi) = max(epsilons(yi),e_candidate);
                case 'prod'
                    if isnan(epsilons(yi))
                        epsilons(yi) = e_candidate;
                    else
                        epsilons(yi) = epsilons(yi)*e_candidate;
                    end
                case 'sum'
                    if isnan(epsilons(yi))
                        epsilons(yi) = e_candidate;
                    else
                        epsilons(yi) = epsilons(yi)+e_candidate;
                    end
                case 'mean'
                    if isnan(epsilons(yi))
                        epsilons(yi) = e_candidate/numel(nb);
                    else
                        epsilons(yi) = epsilons(yi)+e_candidate/numel(nb);
                    end
                    
                otherwise 
                    error('No valid mode');
            end
        end
    end

end

