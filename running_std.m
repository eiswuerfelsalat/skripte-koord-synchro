function [ s ] = running_std( x ,width )
%RUNNING_STD Summary of this function goes here
%   Detailed explanation goes here

%%
x = reshape(x,[],1);

% var = n/(n-1)*(<x^2>-<x>^2)
% std^2 = var

m  = running_mean(x, width);    % <x>
m2 = running_mean(x.^2, width); % <x^2>

factor = numel(x)/(numel(x)-1);
s2 = factor * (m2 - m.^2);

s = sqrt(s2);
s(1:width-1) = s(width-1);
s(end-width+1:end) = s(end-width+1);
end

