function [ H ] = ShannonEntropy( X , n )
% 
% [ H ] = ShannonEntropy( X , n )
%
% calculates the Shannon Entropy
% H = -1* sum( p .*log2(p) ) 
%
% X: datenreihe
% n: Anzahl der Bins, in die die Daten eingeteilt werden
%    siehe hist(X,n)
% n ist default 2*numel(X)
%
% extrema: H -> 0  : periodisch
%          H -> max: gleichverteilt
%

if nargin < 2, n = 2*numel(X); end;

    anz = hist(X,n);
    ind = ( anz ~= 0 ); 
    
    % ignore values with p = 0
    % that's ok, because lim p->0 of p*log(p) = 0
    p = anz(ind) / numel(X);
    H = -1* sum( p .*log2(p) ) ;

end

