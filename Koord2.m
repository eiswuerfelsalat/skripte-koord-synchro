function [index_R,index_A] = Koord2(A,R)
% [index_R,index_A] = Koord(A,R)
%   t_R  :   time of R as colon
%   AR_dist: time between R and former A, non-negative
%
%  Beats before the first and after the last bearth are ignored.
%
% t_R = R(index_R);
% AR_dist = R(index_R)-A(index_A); % >= 0
% RA_dist = AR_dist-aa(index_A); % < 0
%
% dots = [R(index_R) AR_dist ; ...
%         R(index_R) RA_dist ];
%
% Koordigram:
% plot(t_R,AR_dist,'.', t_R,RA_dist,'.');
%% 

R = reshape(R,numel(R),1); % enforce colon vector
A = reshape(A,numel(A),1); % enforce colon vector

if ~issorted(R)
    R = sort(R);
end
if ~issorted(A)
    A = sort(A);
end

index_R = find(A(1) <= R & R < A(end));
index_A = NaN(size(index_R));

iA_before = 1;
iA_after = 2;

iRvals = index_R'; % transposing is needed, so that
                         % 'for' takes only one element
for i = 1:numel(iRvals);
    iiR = iRvals(i);
    while A(iA_after) <= R(iiR)
        iA_before = iA_after;
        iA_after = iA_before +1;
    end
    index_A(i) = iA_before;
end

%t_R = R(index_R);
%AR_dist = R(index_R)-A(index_A); % >= 0

%aa = diff(A);
%RA_dist = AR_dist-aa(index_A); % < 0
%dots = [R(index_R) AR_dist ; ...
%        R(index_R) RA_dist ];
