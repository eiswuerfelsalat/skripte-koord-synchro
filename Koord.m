function [dots, iA, iR, index_A, index_R, NR, NA] = Koord(A,R)
% [dots, iA, iR] = Koord(A,R)
%
%   A, R :   should be sorted
%
%   iA   :   index of A before (upper part) or after (bottom part) R
%            DO NOT use this for indexing in diff(A), see below and Koord2!
%   dots :   m-by-2 matrix 
%            1. coloumn is time of beat
%            2. is difference to A
%   upper part of the Koordigram can be extracted with 
%   iUpper = dots(:,2) >= 0;
%
%  dots = [R(iR) R(iR)-A(iA)]
%
%
% aa = diff(A);
% [~, ~, iR, index_A] = Koord(A,R)
% iaa = [index_A; index_A];
% plot(R(iR),aa(iaa));
%

R = reshape(R,numel(R),1); % enforce colon vector
A = reshape(A,numel(A),1); % enforce colon vector

[index_R,index_A] = Koord2(A,R);
aa = diff(A);
AR_dist = R(index_R) - A(index_A); % >= 0
RA_dist = AR_dist - aa(index_A); % < 0

dots = [R(index_R) AR_dist ; ...
        R(index_R) RA_dist ]; % should be N-x-2-matrix

if nargout >= 2
    iA = [index_A; index_A+1];
end

if nargout >= 3
    iR = [index_R; index_R];
end

if nargout >= 4
    NR = numel(index_R);
end

if nargout >= 5
    NA = numel(index_A);
end

return