% Detektion der Koordination nach P. Engel, G. Hildebrandt und H.-G. Scholz
% (1967)
% Die Messung der Phasenkoppelung zwischen Herzschlag
% und Atmung beim Menschen
% mit einem neuen Koinzidenzme�ger�t
% 
% die Methode wurde auch schon bei Bucher und B�ttig (1960) angewandt
% Zum Mechanismus der pulssynchronen Atmung. Helv. physiol, pharmacol.
% Acta 18, 25--34 (1960).
%
% hier wird die Rolle von Herz und Atmung bezogen auf das Paper vertauscht
%

load model4;
t_r = t_r(10:end);
[dots, Aind, ~, iA, iR] = Koord(t_a,t_r);
iuA = min(iA):max(iA);
nA = numel(iuA)-1;
N = numel(iR); % number of heartbeats
R = t_r(iR);
A = t_a(iuA);

subplot(121)
plot(dots(1:N,1),dots(1:N,2),'.')
xlabel('Zeit t');
ylabel('Offset \Delta t')

% instead of setting the number of bins, the bin width and the maximum
% is given.
binMax = 6;
binWidth = 0.3;
edges = 0:binWidth:binMax;

subplot(122)
h = histogram(dots(1:N,2),edges);
E = N/h.NumBins; % expected/mean number of beats per bin
% 'Normalization' � Type of normalization
% 'count' (default) | 'probability' | 'countdensity' | 'pdf' | 'cumcount' | 'cdf'
fprintf('%i beats in plot\n',N)
fprintf('%.1f avg beats per class\n',E)
disp('------------')
% histogram(diff(A),edges)

% Korrektur aufgrund der nicht konstanten Atemzugsdauer
% heartbeats
counts = h.BinCounts;
nC = numel(counts);
% breaths
abortingA = histcounts(diff(A),edges,'Normalization','count');

nCorr = cumsum(abortingA) - 0.5*abortingA;

cCorr = counts * nA ./ (nA-nCorr);
Ecorr = sum(cCorr)/nC;

chi2 = sum((cCorr - Ecorr).^2) ./ Ecorr
% formula (1) in the paper
% ciritical value: (chi2 must be bigger for coordination)
chi2crit = chi2inv(0.95,nC-1)

C = chi2 / Ecorr
Ccrit = chi2crit / Ecorr

hold on;
stairs(edges,[cCorr cCorr(end)],':')
plot([0 binMax],E*[1 1],'-')
plot([0 binMax],Ecorr*[1 1],'-')
hold off

