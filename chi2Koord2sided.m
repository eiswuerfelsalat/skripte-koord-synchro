% Detektion der Koordination nach P. Engel, G. Hildebrandt und H.-G. Scholz
% (1967)
% Die Messung der Phasenkoppelung zwischen Herzschlag
% und Atmung beim Menschen
% mit einem neuen Koinzidenzme�ger�t
% 
% die Methode wurde auch schon bei Bucher und B�ttig (1960) angewandt
% Zum Mechanismus der pulssynchronen Atmung. Helv. physiol, pharmacol.
% Acta 18, 25--34 (1960).
%
% hier wird die Rolle von Herz und Atmung bezogen auf das Paper vertauscht
%

function [ chi2, chi2crit, Ecorr, nCorr ] = chi2Koord2sided( dots2,AA,k,z )
% chi2 = chi2Koord2sided( dots2, AAintervals, z, k )
%
% dots2     delta t values, dots(:,2)
% A         breath to breath intervals
% k         2*k is the number of used classes in histcount
% z         maximum class edge, default: mean(AA)
%
% chi2 is dependent on number of bins and number of given beats
% C depends only on the number of bins
% C = chi2 / meanPerBin = chi2 * k/N
%
% plot when no return value

if nargin < 4
    z = mean(AA);
end


doPlot = nargout == 0;
edges = linspace(-z,z,2*k+1); % with 2*k+1 "0" is always included

N = numel(dots2);
nA = numel(AA);
counts = histcounts(dots2,edges);
nC = numel(edges)-1;
E = N/nC; % expected/mean number of beats per bin


if E < 20
    fprintf('%i beats in plot\n',N)
    fprintf('%.1f avg beats per class\n',E)
    warning('there should be at least 20 points per class')
end

% Korrektur aufgrund der nicht konstanten Atemzugsdauer heartbeats
% number of intervals ending in each class
abortingA = histcounts(AA,edges(k+1:end),'Normalization','count');
nCorr = cumsum(abortingA) - 0.5*abortingA;
nCorr = [nCorr(end:-1:1) nCorr];
cCorr = counts * nA ./ (nA-nCorr);
Ecorr = sum(cCorr)/nC;

chi2 = sum((cCorr - Ecorr).^2) ./ Ecorr;
% formula (1) in the paper
% ciritical value: (chi2 must be bigger for coordination)
chi2crit = chi2inv(0.95,nC-1);

C = chi2 / Ecorr;
Ccrit = chi2crit / Ecorr;

if (doPlot)
    histogram(dots2,edges);
    hold on;
    stairs(edges,[cCorr cCorr(end)],':')
    plot([min(edges) max(edges)],E*[1 1],'-')
    plot([min(edges) max(edges)],Ecorr*[1 1],':')
    hold off
    xlim([min(edges) max(edges)]);
end

end

