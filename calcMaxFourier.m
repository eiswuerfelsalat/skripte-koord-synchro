function [maxFval,maxF,fourier,f,usedZ,zFieldVal,zFieldF] = calcMaxFourier(koord,dy, fBorder, zVals)
% [maxFval,maxF,fourier,f,usedZ] = calcMaxFourier(koord,dy, fborder)
% koord:   color coded coordigram, 1. dimension is dy and 2. dimension is 
%          absolute time
% dy:      samplepoints of R-A axis (1. Dimension of koord)
% fBorder: interval [min, max] where to search maximum fourier component
% zVals:   values of z (to vary length of dy to overcome periodic
%          fluctuation due to its length). max(z)-min(z) should be at least
%          half of the mean beat to beat interval.
%
% maxFval: absolute value of maximum fourier coefficient
% maxF :   frequency of maximum fourier coefficient
% fourier: matrix of all coefficients for all times. This is meaningfull 
%          for a single z only.
% f:       frequencies which where considered. This is meaningfull 
%          for a single z only.
% usedZ:   which z has the max fourier component

if nargin < 4
    % for code which uses this function without z
    warning('The interface of calcMaxFourier has changed, please add zInterval parameter. Otherwise no correction is applied!');
    %zInterval = max(abs(dy));
    usedZ = max(abs(dy));
    [maxFval,maxF,fourier,f] = calcMaxFourier2(koord,dy, fBorder);
    return
end

[M,N] = size(koord);
fourier = zeros(M,N);
maxFval = zeros(1,N);
maxF    = zeros(1,N);
usedZ   = zeros(1,N);

zFieldVal  = zeros(numel(zVals),N);
zFieldF    = zeros(numel(zVals),N);
for zInd = 1:numel(zVals)
    z = zVals(zInd);
    ii = abs(dy) <= z;
    %zInterval = max(dy(ii)) * [1 1];
    [Fmax_tmp,fmax_tmp,fourier,f] = calcMaxFourier2(koord(ii,:),dy(ii), fBorder); % pd must be col. vector
    zFieldVal(zInd,:) = Fmax_tmp;
    zFieldF(zInd,:) = fmax_tmp;
end

[maxFval, maxZind] = max(zFieldVal,[],1);
usedZ = zVals(maxZind);
for i = 1:N
    maxF(i) = zFieldF(maxZind(i),i);
end

