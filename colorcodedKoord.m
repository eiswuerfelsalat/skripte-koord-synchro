function [ f1, uniqueA_index ] = colorcodedKoord( y, Aind, winlen, yAxis, bandweite )
% [f,uniqueA_index]  = colorcodedKoord( y, Aind, winlen, yAxis, bandweite )
%
% y = dots(:,2)
% Aind: index des zugeh�rigen Atemzugs zu einem Eintrag in dots
% winlen: Anzahl der Atemz�ge
% yAxis = -ymin:ystep:ymax f�r die Verteilung
% bandweite: Breite des Kernelschätzers
%
% f      : Verteilungsfunktion
% A_index: A-Index f�r den f berechnet wurde (x-Achse beim plot)
%          (Anfang des Fensters der L�nge winlen)
%
% Beispiel:
% fensterRadius=5; % Anzahl der Atemz�ge f�r das gleitenden Fensters, indem 
%                  % die Vereilungenen bestimmt werden
% bandbreite=0.1;  % Bandbreite des Kernschätzers zur Bestimmung der 
%                  % Verteilungen (Zweifaches des Abtastschrittes der Atmung
% yAxis = -8:0.05:8; % Einteilung der y-Achse
% 
% [verteilung,usedA] = colorcodedKoord(dots(:,2) ,Aind,...
%                                    fensterRadius, yAxis, bandbreite);
% h=imagesc(t_a(usedA), yAxis, verteilung);
% colormap jet; caxis([0 1]); colorbar;
% title('colored coordigram')
% xlabel('Zeit t');
% ylabel('Offset \Delta t')
% 
%
%

uniqueA_index = unique(Aind,'sorted');
N = numel(uniqueA_index);
f1 = zeros(numel(yAxis),N);
%normfactor = nan(N,1);
for avi = 1:N
    
    ii = (Aind == uniqueA_index(avi));
    for i = 1:(winlen-1)
        ii = ii | (Aind == uniqueA_index(avi) + i);
    end
    
    x = y(ii);
    
    if ~isempty(x)
        v = numel(x)*sqrt(2*pi)*bandweite/winlen; % maxima = 1
        %normfactor(avi) = numel(x);
        f1(:,avi) = v*ksdensity(x,yAxis,'width',bandweite);
    end
    
end

end