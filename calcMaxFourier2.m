function [maxFval,maxF,fourier,f] = calcMaxFourier2(koord,dy, fborder, z)
% [maxFval,maxF,fourier,f] = calcMaxFourier(koord,dy, fborder)
% koord:   color coded coordigram, 1. dimension is dy and 2. dimension is 
%          absolute time
% maxFval: absolute value of maximum fourier coefficient
% maxF :   frequency of maximum fourier coefficient
% fourier: matrix of all coefficients for all times
% f:       frequencies which where considered
%

fourier = zeros(size(koord));
maxFval = zeros(1,size(koord,2));
maxF    = zeros(1,size(koord,2));

T = max(dy)-min(dy);
f = (0:numel(dy)-1)/T;
%     ff = 0:0.01:max(f);
f_I = min(fborder) <= f & f <= max(fborder);

%ff2= 0.8:0.01:1.5; % Bereich, in dem das Maximum gesucht werden soll

fs = 1/(dy(2)-dy(1));

for t0 = 1:size(koord,2)
    x = koord(:,t0);
    %fx = abs(fft(x)) * diff(dy(1:2)); % Fourier komponenten,normiert
    fx = periodogram(x,[],f,fs)'/T; % more stable results, but...
    fx = 2*sqrt(fx); % returns power density spectrum, so this is 1/2 * f^2
    
    fourier(:,t0) = fx; % ins Fourier-Feld eintragen
    
    %fx(1) = 0; % const-offset Null setzen
    %ffx = spline(f,fx,ff2);  % interpoliere Fourierverteilung
    [fmax,find]=max(fx(f_I));
    maxFval(t0) = fmax; % Komponente mit höchstem Beitrag
    f_ff = f(f_I);
    maxF(t0) = f_ff(find);
end
