function [ D ] = Dsymmetric( p, q )
% takes one 1-by-n vector p and a m-by-n matrix q
% returns m-by-1 vector of distances

    D = zeros(size(q,1),1);
    for j = 1:size(q,1)
        ii = q(j,:)~=0 & p~=0;
        a = p(ii);
        b = q(j,ii);
        %a = a./sum(a);
        %b = b./sum(b);
        D(j) = 0.5*(a*log2(a'./b') + b*log2(b'./a'));
        %D(j) = sum( p(ii) .*log2(p(ii)./q(j,ii)) ) ;
    end
end