% devel f�r chi^2-Methode
clc; clear all;
load model4;
t_r = t_r(10:end);
[dots, Aind, ~, iA, iR] = Koord(t_a,t_r);
iuA = min(iA):max(iA);
nA = numel(iuA);
N = numel(iR); % number of heartbeats
R = t_r(iR);
A = t_a(iuA);

subplot(121)
plot(dots(1:N,1),dots(1:N,2),'.')
xlabel('Zeit t');
ylabel('Offset \Delta t')

% instead of setting the number of bins, the bin width and the maximum
% is given.
binMax = 6;
binWidth = 0.3;
edges = 0:binWidth:binMax;


subplot(122)
%chi2Koord(dots(1:N,2),diff(A),edges);
chi2 = chi2Koord(dots(1:N,2),diff(A),edges)

chi2Koord2sided(dots(:,2),diff(A),edges)



