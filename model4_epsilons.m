%% Epsilon Methode

%% Verwendete Daten
% Die Daten wurden mit dem Modell 4 simmuliert und die Koordination soll
% nun mit der Epsilon-Methode analysiert werden.

load model4;
[dots, Aind] = Koord(t_a,t_r);
plot(dots(:,1),dots(:,2),'.')
xlabel('Zeit t');
ylabel('Offset \Delta t')


%%
epsilons = calculate_epsilons(dots(:,2),Aind);
histogram(epsilons, 0:0.05:1)

%%
ii = epsilons < 0.15;
plot(dots( :,1), dots( :,2), 'c.', ...
     dots(ii,1), dots(ii,2), 'b.' )


%% Einfache Anwendung
% Als Maß der Koordination wird der Anteil der Herzschläge gezählt, die
% innerhalb eines Epsilonschlauch mit ihren Nachbarn liegt.
% Für eine bessere Interpretation und Vergleichbarkeit mit anderen
% Messungen wird der Wert als relative Größe (in Prozent) angegeben.

eps_vals = 0.01:0.05:0.7;
Cp1 = zeros(size(eps_vals));
for i = 1:numel(eps_vals)
    ii = epsilons < eps_vals(i);
    Cp1(i) = sum(ii)/numel(ii)*100;
end
   
plot(eps_vals, Cp1,'.:')
xlabel('Epsilon value \epsilon')
ylabel('koordinated beats in %')
%%
% Bei geringerer Schrittweite der Epsilonwerte erscheint die Kurve stufig
% aufgrund der Zeitdiskretisierung der ursprünglichen Daten.

%% weitere Optionen

epsilons2 = calculate_epsilons(dots(:,2),Aind,3,'min');
epsilons3 = calculate_epsilons(dots(:,2),Aind,3,'max');
histogram(epsilons2, 0:0.1:1)

%%
% calculate percentage of coordinated beats

Cp2 = zeros(size(eps_vals));
Cp3 = zeros(size(eps_vals));
for i = 1:numel(eps_vals)
    ii = epsilons2 < eps_vals(i);
    Cp2(i) = sum(ii)/numel(ii)*100;
    
    ii = epsilons3 < eps_vals(i);
    Cp3(i) = sum(ii)/numel(ii)*100;
end
   
plot( eps_vals, Cp1,'b.:',...
      eps_vals, Cp2,'g.:',...
      eps_vals, Cp3,'c.:')
xlabel('Epsilon value \epsilon')
ylabel('coordinated beats in %')

legend({'N = 1','N = 2 min','N = 2 max'},'Location','se')

%%
% In der letzten Darstellung erkennt man gut den Einfluss der Modi 'min'
% und 'max'. Wird bei den auf einen Herzschlag folgenden Atemz�gen jeweils
% der Minimalwert
% bestimmt, so erhöht sich die Anzahl der koordinierten Schläge. Im
% 'max'-Modus muss für die angegebene Anzahl von Atemzügen jeweils ein
% Herzschlag im entsprechenden Epsilonschlauch liegen. Die Bedingung ist
% strikter und die Anzahl der koordinierten Schläge fällt geringer aus.

%% Zeitabhängigkeit

winLen = 60; % sekunden
winStep = min(dots(:,1)):20:max(dots(:,1));
Pe = zeros(size(winStep));

epsTh = 0.15;
for i = 1:numel(winStep)
    ii =  winStep(i)-winLen/2 <= dots(:,1) ...
          & dots(:,1) <= winStep(i)+winLen/2;
    Pe(i) = sum(epsilons(ii)<=epsTh)/sum(ii);
end

subplot(211)
ii = epsilons < 0.15;
plot(dots( :,1), dots( :,2), 'c.', ...
     dots(ii,1), dots(ii,2), 'b.' )

hold on
h = imagesc(winStep,[-10 -8],repmat(Pe,2,1));
colormap jet; caxis([0 1]*0.8); colorbar;
set(gca,'Position',get(gca,'Position').*[1 1 0.9 1]); % Trick to align with colorbar

plot(mean(winStep(1:2))+[0 winLen],[7 7],'r+-')
xlim([min(winStep) max(winStep)]); 
ylim([-10 10])
hold off 

 
subplot(212)
plot(winStep,100*Pe);
set(gca,'Position',get(gca,'Position').*[1 1 0.9 1]); % Trick to align with colorbar
xlabel('Zeit t'); ylim([0 100])
ylabel('coordinated beats in %')

