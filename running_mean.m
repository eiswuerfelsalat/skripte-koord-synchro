function [ y ] = running_mean( x,win )
%running_mean calculates average almost like smooth(x)
% returns colon vector with the same number of elements like x;
x = reshape(x,[],1);

b = (1/win)*ones(1,win);
y = filter(b,1,[x(1)*ones(win-1,1); x; x(end)*ones(win-1,1)]);
y = y(win+fix(win/2):end-win+1+fix(win/2));

end

