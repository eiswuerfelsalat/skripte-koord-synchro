%% Recurrenz

%% Verwendete Daten
% Die Daten wurden mit dem Modell 4 simmuliert und die Koordination soll
% nun mittels Rekurrenz-Methode analysiert werden.

load model4;
[dots, Aind] = Koord(t_a,t_r);
plot(dots(:,1),dots(:,2),'.')
xlabel('Zeit t');
ylabel('Offset \Delta t')


%% Farbdarstellung des Koordigramms

fensterRadius=3; % Anzahl der Atemzüge für das gleitenden Fensters, indem 
                 % die Vereilungenen bestimmt werden
bandbreite=0.1;  % Bandbreite des Kernschätzers zur Bestimmung der 
                 % Verteilungen (Zweifaches des Abtastschrittes der Atmung
yAxis = -8:0.05:8;

[verteilung,Avals] = colorcodedKoord(dots(:,2), Aind, fensterRadius, yAxis, bandbreite);

h=pcolor(t_a(Avals), yAxis, verteilung);
set(h, 'EdgeColor', 'none'); caxis([0 1]); colorbar;
title('colored coordigram')
xlabel('Zeit t');
ylabel('Offset \Delta t')

%%

A = squareform(pdist(verteilung',@Dsymmetric));

%h = pcolor(t_a(Avals),t_a(Avals),A);
h = pcolor(t_a(Avals),t_a(Avals),-log(A));
set(h, 'EdgeColor', 'none'); %caxis([0 1]); 
axis square; colorbar;
title('Similarity of breaths')

%%
histogram(A)

%%

winLen = 60; % sekunden
winStep = min(t_a):20:max(t_a);
Pe = zeros(size(winStep));

thresh = 90;
for i = 1:numel(winStep)
    ii =  winStep(i)-winLen/2 <= t_a(Avals) ...
          & t_a(Avals) <= winStep(i)+winLen/2;
    Pe(i) = sum(sum(A(ii,ii) >= thresh))/(sum(ii)^2);
end

clf;
subplot(211)
plot(dots( :,1), dots( :,2), 'k.' )
hold on
h = imagesc(winStep,[7 -7],repmat(Pe,2,1));
set(h, 'AlphaData', 0.4);
uistack(h,'bottom');

plot(mean(winStep(1:2))+[0 winLen],[7 7],'r+-')
xlim([min(winStep) max(winStep)]); colorbar;
colormap jet; caxis([0 1]);
%load cMap; colormap(cMap); caxis([0 1]);

hold off 
set(gca,'Position',get(gca,'Position').*[1 1 0.9 1]); % Trick to align with colorbar
ylim([-1 1]*8)

subplot(212)
plot(winStep,100*Pe);
set(gca,'Position',get(gca,'Position').*[1 1 0.9 1]); % Trick to align with colorbar
xlabel('Zeit t'); ylim([0 100])
ylabel('coordination')



