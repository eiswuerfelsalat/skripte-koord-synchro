function [ Vabs, Vang, Tmax ] = KoordLambda( dt, Tvals, iA, w)
% [ Vabs, Vang, Tmax ] = KoordLambda( dt, Tvals )
%
% es wird für die Werte T das Maximum von abs(v) gesucht
% v = mean(exp(2i*pi* dt / T))
% v = Vabs * exp(1i* Vang)
%
% [ Vabs, Vang, Tmax ] = KoordLambda( dt, Tvals, iA, w)
%
%  gibt für jeden Atemzug (min(iA):max(iA)) einen Wert zurück

if nargin == 2
    
    v = zeros(size(Tvals));
    for i = 1:numel(Tvals)
        T = Tvals(i);
        v(i) = mean(exp(2i*pi* dt / T)); % complex
    end

    [Vabs,J] = max(abs(v));
    Vang = angle(v(J));
    Tmax = Tvals(J);
    return
end

%uiA = min(iA):max(iA);
uiA = unique(iA);
N = numel(uiA);

Vabs = zeros(N,1);
Vang = zeros(N,1);
Tmax = zeros(N,1);

for j = 1:N
    ciA = uiA(j);
    
    ii = iA == ciA;
    for k = 1:(w-1)
        ii = ii | iA == (ciA + k);
    end
    
    v = zeros(size(Tvals));
    for i = 1:numel(Tvals)
        T = Tvals(i);
        v(i) = mean(exp(2i*pi* dt(ii) / T)); % complex
    end

    [Vabs(j),J] = max(abs(v));
    Vang(j) = angle(v(J));
    Tmax(j) = Tvals(J);
end

end

