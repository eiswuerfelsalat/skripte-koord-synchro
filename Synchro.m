function [ phi, iA, dots ] = Synchro( A, R )
%SYNCHRO 
% [ phi, iA, koord ] = Synchro( A, R )
%
%   A:    Einatem-Zeitpunkte
%   R:    Herzschlag-Zeitpunkte
%
%   phi:  Phase für R (zwei Spalten, Plot s.u.)
%   koord: Koordigram, dass als Basis berechnet wurde
%   
%   plot(phi(:,1),phi(:,2),'k.');
%
    A = A(:)';
    R = R(:)';
    [dots, iA] = Koord(A,R);

    AA = diff([A max(R)]);
    iG = dots(:,2) >= 0;
    R = dots(iG,1);
    iA = iA(iG);
    
    A2 = A(iA)';
    phi = [R, (R - A2)./(AA(iA)')];

end

